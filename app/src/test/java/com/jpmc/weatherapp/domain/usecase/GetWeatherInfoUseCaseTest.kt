package com.jpmc.weatherapp.domain.usecase

import com.jpmc.weatherapp.data.local.StateStoreManager
import com.jpmc.weatherapp.data.model.GeoDirectResponse
import com.jpmc.weatherapp.data.model.GeoDirectResponseItem
import com.jpmc.weatherapp.data.model.LocalNames
import com.jpmc.weatherapp.data.repository.WeatherRepositoryImpl
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class GetWeatherInfoUseCaseTest {

    @Mock
    lateinit var weatherRepositoryImpl: WeatherRepositoryImpl

    @Mock
    lateinit var stateStoreManager: StateStoreManager

    private lateinit var getGeoCodeUseCase: GetGeoCodeUseCase



    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        getGeoCodeUseCase = GetGeoCodeUseCase(weatherRepositoryImpl, stateStoreManager)
    }

    @Test
    fun `test invoke() should return Location`() = runBlocking {
        // Mock data
        val geocodeDirectResponseItem = GeoDirectResponseItem(country = "USA", lat = -88.00, local_names = LocalNames("en"), lon = 21.00, name ="Name", state = "Texas")
        val directLocation = GeoDirectResponse()
        directLocation.add(geocodeDirectResponseItem)
        Mockito.`when`(weatherRepositoryImpl.getGeoCode("Plano")).thenReturn(directLocation)

        // Call the function to be tested
        val result = getGeoCodeUseCase.invoke("Plano")

        // Verify that the result is the same as the mocked data
        assertEquals(geocodeDirectResponseItem.lat, result.latitude)
    }
}