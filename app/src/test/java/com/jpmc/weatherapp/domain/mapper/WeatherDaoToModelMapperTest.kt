package com.jpmc.weatherapp.domain.mapper

import com.jpmc.weatherapp.data.model.Coord
import com.jpmc.weatherapp.data.model.Main
import com.jpmc.weatherapp.data.model.Weather
import com.jpmc.weatherapp.data.model.WeatherInfoResponse
import com.jpmc.weatherapp.data.model.Wind
import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations

class WeatherDaoToModelMapperTest {
    @Mock
    lateinit var mockWeatherInfoResponse: WeatherInfoResponse

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun testMapping() {
        // Mock WeatherInfoResponse
        val coord = Coord(40.7128, -74.0060)
        val weather = Weather("clear sky", "01d", 800, "Clear")
        val main = Main(12.0, 10, 60, 12, 60,10.0,20.0,10.0)
        val wind = Wind(3,10.0,3.5)
        `when`(mockWeatherInfoResponse.coord).thenReturn(coord)
        `when`(mockWeatherInfoResponse.name).thenReturn("New York")
        `when`(mockWeatherInfoResponse.weather).thenReturn(listOf(weather))
        `when`(mockWeatherInfoResponse.main).thenReturn(main)
        `when`(mockWeatherInfoResponse.wind).thenReturn(wind)

        // Create the mapper instance
        val mapper = WeatherDaoToModelMapper()

        // Perform the mapping
        val result = mapper.mapFrom(mockWeatherInfoResponse)

        // Verify the result
        assertEquals("New York", result.cityName)
        assertEquals(40.7128, result.location.latitude, 0.001)
        assertEquals(-74.0060, result.location.longitude, 0.001)
        assertEquals("clear sky", result.weatherDescription)
        assertEquals(800, result.weatherID)
        assertEquals("Clear", result.weatherMain)
        assertEquals(10.0, result.temp, 0.001)
        assertEquals(10.0, result.tempMin, 0.001)
        assertEquals(20.0, result.tempMax, 0.001)
        assertEquals(12.0, result.feelsLike, 0.001)
        assertEquals("01d", result.iconID)
        assertEquals(60, result.humidity)
        assertEquals(3.5, result.windSpeed, 0.001)
    }

}