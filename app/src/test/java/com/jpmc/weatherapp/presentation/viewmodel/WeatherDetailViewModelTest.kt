package com.jpmc.weatherapp.presentation.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.jpmc.weatherapp.MainCoroutineScopeRule
import com.jpmc.weatherapp.domain.model.Location
import com.jpmc.weatherapp.domain.model.WeatherInfoModel
import com.jpmc.weatherapp.domain.usecase.GetGeoCodeUseCase
import com.jpmc.weatherapp.domain.usecase.GetWeatherInfoUseCase
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentCaptor
import org.mockito.Captor
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.Mockito.verifyNoMoreInteractions
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.capture

@RunWith(MockitoJUnitRunner::class)
internal class WeatherDetailViewModelTest {

    @Captor
    private lateinit var weatherInfoModelCaptor: ArgumentCaptor<WeatherDetailViewModel.MainScreenState>

    // This rule swaps the background executor used by the Architecture Components
    // with a different one which executes each task synchronously.
    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    // This allows testing of code using coroutines without the need to manually advance time
    @OptIn(ExperimentalCoroutinesApi::class)
    @get:Rule
    var coroutinesTestRule = MainCoroutineScopeRule()

    // Mocking dependencies
    @Mock
    private lateinit var getWeatherInfoUseCase: GetWeatherInfoUseCase

    @Mock
    private lateinit var getGeoCodeUseCase: GetGeoCodeUseCase

    @Mock
    private lateinit var observer: Observer<WeatherDetailViewModel.MainScreenState>

    // Class under test
    private lateinit var viewModel: WeatherDetailViewModel

    //
    @Before
    fun setUp() {
        viewModel = WeatherDetailViewModel(getWeatherInfoUseCase, getGeoCodeUseCase)
        // Observe LiveData in ViewModel
        viewModel.liveData.observeForever(observer)
    }

    @After
    fun tearDown() {

    }

    @Test
    fun `test getting data from coordinates`() = runBlocking {
        // Prepare
        val location = Location(0.0, 0.0)
        val weatherInfoModel = WeatherInfoModel(
            location = Location(10.90, 11.34),
            cityName = "Plano",
            weatherDescription = "rainy",
            weatherID = 1,
            weatherMain = "cloud",
            iconID = "12d",
            temp = 123.0,
            tempMin = 99.0,
            tempMax = 199.0,
            feelsLike = 111.0,
            windSpeed = 34.0,
            humidity = 123
        )
        `when`(getWeatherInfoUseCase.invoke(location)).thenReturn(weatherInfoModel)

        // Act
        viewModel.getDataFromCoordinate(location)

        // Verify
        verify(observer).onChanged(capture(weatherInfoModelCaptor))
        val mainScreenState = weatherInfoModelCaptor.value
        if (mainScreenState is WeatherDetailViewModel.MainScreenState.Success) {
            assertEquals(weatherInfoModel.cityName, mainScreenState.data.cityName)
        }
        verifyNoMoreInteractions(observer)
    }
}