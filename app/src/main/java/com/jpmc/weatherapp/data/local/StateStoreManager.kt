package com.jpmc.weatherapp.data.local

import android.content.SharedPreferences
import com.jpmc.weatherapp.data.IStateStore
import com.jpmc.weatherapp.domain.model.Location
import javax.inject.Inject

class StateStoreManager @Inject constructor(private val preferences: SharedPreferences) : IStateStore{
    override fun saveLocation(lat: Float, long: Float) {
        preferences.edit()
            .putFloat("lat", lat)
            .putFloat("long", long)
            .apply()
    }

    override fun getLocation(): Location {
        val lat = preferences.getFloat("lat", 0f).toDouble()
        val long = preferences.getFloat("long", 0f).toDouble()
        return Location(lat, long)
    }

}