package com.jpmc.weatherapp.data

import com.jpmc.weatherapp.domain.model.Location

interface IStateStore {

    fun saveLocation(lat: Float, long: Float)

    fun getLocation(): Location
}