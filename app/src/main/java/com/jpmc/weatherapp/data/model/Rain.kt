package com.jpmc.weatherapp.data.model

data class Rain(
    val `1h`: Double
)