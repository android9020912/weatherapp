package com.jpmc.weatherapp.data.model

data class Coord(
    val lat: Double,
    val lon: Double
)