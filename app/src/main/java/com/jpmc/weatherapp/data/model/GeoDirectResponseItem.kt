package com.jpmc.weatherapp.data.model

data class GeoDirectResponseItem(
    val country: String,
    val lat: Double,
    val local_names: LocalNames,
    val lon: Double,
    val name: String,
    val state: String
)