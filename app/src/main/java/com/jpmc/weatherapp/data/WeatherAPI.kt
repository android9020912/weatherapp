package com.jpmc.weatherapp.data

import com.jpmc.weatherapp.data.model.GeoDirectResponse
import com.jpmc.weatherapp.data.model.WeatherInfoResponse
import com.jpmc.weatherapp.utils.Constants
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherAPI {
    @GET("geo/1.0/direct")
    suspend fun getGeoCode(
        @Query("q", encoded = true) cityName: String,
        @Query("limit") limit: Int = 5,
        @Query("appid") appKey: String = Constants.API_KEY
    ): GeoDirectResponse

    @GET("data/2.5/weather")
    suspend fun getWeatherInfo(
        @Query("lat") lat: String,
        @Query("lon") lon: String,
        @Query("appid") appKey: String = Constants.API_KEY
    ): WeatherInfoResponse

}
