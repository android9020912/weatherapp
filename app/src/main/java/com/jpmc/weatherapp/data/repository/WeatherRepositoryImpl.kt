package com.jpmc.weatherapp.data.repository

import com.jpmc.weatherapp.data.WeatherAPI
import com.jpmc.weatherapp.data.model.GeoDirectResponse
import com.jpmc.weatherapp.data.model.WeatherInfoResponse
import com.jpmc.weatherapp.domain.repository.IWeatherRepository
import com.jpmc.weatherapp.domain.model.Location
import javax.inject.Inject

class WeatherRepositoryImpl @Inject constructor(private val weatherAPI: WeatherAPI) :
    IWeatherRepository {
    override suspend fun getWeatherDetails(location: Location): WeatherInfoResponse {
        return weatherAPI.getWeatherInfo(location.latitude.toString(), location.longitude.toString())
    }

    override suspend fun getGeoCode(cityName: String): GeoDirectResponse {
        return weatherAPI.getGeoCode(cityName.replace(" ", ","))
    }
}