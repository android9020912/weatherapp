package com.jpmc.weatherapp.utils

fun Double.kelvinToFahrenheit(): Int {
    return ((this - 273.15) * 9 / 5 + 32).toInt()
}