package com.jpmc.weatherapp.utils

object Constants {
    const val BASE_URL: String = "https://api.openweathermap.org/"
    const val API_KEY: String = "9e7f283505ea65a63a5a941e6f135517"
    const val ICON_URL: String = "https://openweathermap.org/img/wn/"
}