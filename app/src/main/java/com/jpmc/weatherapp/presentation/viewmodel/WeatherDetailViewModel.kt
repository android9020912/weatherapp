package com.jpmc.weatherapp.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.jpmc.weatherapp.domain.model.Location
import com.jpmc.weatherapp.domain.model.WeatherInfoModel
import com.jpmc.weatherapp.domain.usecase.GetGeoCodeUseCase
import com.jpmc.weatherapp.domain.usecase.GetWeatherInfoUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject
@HiltViewModel
class WeatherDetailViewModel @Inject constructor(
    private val getWeatherInfoUseCase: GetWeatherInfoUseCase,
    private val getGeoCodeUseCase: GetGeoCodeUseCase
) : ViewModel() {

    private val _data = MutableLiveData<MainScreenState>()

    val liveData: LiveData<MainScreenState> = _data

    fun getDataFromCoordinate(location: Location) {
        viewModelScope.launch {
            //here i would prefer to use sealed class for different response like Success, Progress, Error etc to manage all the scenario
            try {
                val data = getWeatherInfoUseCase.invoke(location)
                updateState(MainScreenState.Success(data))
            } catch (e: Exception) {
                updateState(MainScreenState.Error(e.message.toString()))
            }
        }
    }

    fun getLocationByName(cityName: String) {
        viewModelScope.launch {
            try {
                //given more time i will implement validation for empty or invalid input scenarios here
                getDataFromCoordinate(getGeoCodeUseCase.invoke(cityName))
            } catch (e: Exception) {
                updateState(MainScreenState.Error(e.message.toString()))
            }
        }
    }

    private fun updateState(mainScreenState: MainScreenState) {
        _data.postValue(mainScreenState)
    }

    sealed class MainScreenState {
        data class Success(val data: WeatherInfoModel) : MainScreenState()
        data class Error(val message: String) : MainScreenState()
        object Loading : MainScreenState()
    }
}