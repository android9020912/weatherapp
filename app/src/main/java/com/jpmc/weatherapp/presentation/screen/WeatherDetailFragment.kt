package com.jpmc.weatherapp.presentation.screen

import android.Manifest
import android.annotation.SuppressLint
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.widget.SearchView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.bumptech.glide.Glide
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.jpmc.weatherapp.data.local.StateStoreManager
import com.jpmc.weatherapp.databinding.FragmentFirstBinding
import com.jpmc.weatherapp.domain.model.Location
import com.jpmc.weatherapp.presentation.viewmodel.WeatherDetailViewModel
import com.jpmc.weatherapp.utils.kelvinToFahrenheit
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
@AndroidEntryPoint
class WeatherDetailFragment : Fragment() {

    private var _binding: FragmentFirstBinding? = null
    @Inject
    lateinit var stateStoreManager: StateStoreManager
    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!
    private val viewModel by viewModels<WeatherDetailViewModel>()
    private var fusedLocationClient: FusedLocationProviderClient? = null
    private val locationPermissionRequest = registerForActivityResult(
        ActivityResultContracts.RequestPermission()
    ) { permissions ->
        when (permissions) {
            true -> {
                // Only approximate location access granted.
                getCurrentLocationForWeatherData()
                Toast.makeText(context, "Permission Granted", Toast.LENGTH_SHORT).show()
            }

            else -> {
                // we can implement No location access flow here.
                Toast.makeText(context, "Permission Denied", Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentFirstBinding.inflate(inflater, container, false)
        fusedLocationClient = context?.let { LocationServices.getFusedLocationProviderClient(it) }
        return binding.root

    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //given more time we can implement shared preference read and write in
        // different module shared preference manager and inject in fragment as well as use case
        val location = stateStoreManager.getLocation()
        Toast.makeText(context, ""+ location.latitude + "  " + location.longitude, Toast.LENGTH_LONG).show()
        context?.let {
            val sharedPreferences: SharedPreferences =
                it.applicationContext.getSharedPreferences("Location", 0)
            if (sharedPreferences.contains("lat")) {
                val lat = sharedPreferences.getFloat("lat", 0f).toDouble()
                val long = sharedPreferences.getFloat("long", 0f).toDouble()
                viewModel.getDataFromCoordinate(Location(lat, long))
            } else {
                if (ContextCompat.checkSelfPermission(
                        it,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                    ) == PackageManager.PERMISSION_GRANTED
                ) {
                    getCurrentLocationForWeatherData()
                } else {
                    locationPermissionRequest.launch(Manifest.permission.ACCESS_COARSE_LOCATION)
                }

            }
        }
        viewModel.liveData.observe(viewLifecycleOwner) {
            when(it){
                is WeatherDetailViewModel.MainScreenState.Success -> {
                    Log.e("TAG", it.data.toString())
                    binding.cityName.text = it.data.cityName
                    binding.tempMain.text = it.data.temp.kelvinToFahrenheit().toString() + "°F"
                    binding.descMain.text = it.data.weatherDescription.replaceFirstChar(Char::titlecase)
                    binding.tempMin.text = it.data.tempMin.kelvinToFahrenheit().toString() + "°F"
                    binding.tempMax.text = it.data.tempMax.kelvinToFahrenheit().toString() + "°F"
                    binding.feelsLike.text = it.data.feelsLike.kelvinToFahrenheit().toString() + "°F"
                    binding.humidity.text = "${it.data.humidity}%"
                    binding.windSpeed.text = "${it.data.windSpeed}km/h"
                    //Used Glide library here to manage image related operations
                    Glide.with(this).load(it.data.weatherIconURL).into(binding.iconWeather);
                }

                else -> {
                    Log.e("TAG", "ERROR")
                }
            }

        }
        binding.searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                if (!query.isNullOrEmpty()) {
                    Toast.makeText(context, query, Toast.LENGTH_SHORT).show()
                    viewModel.getLocationByName(query)
                    binding.searchView.setQuery("", false)
                    binding.searchView.clearFocus()
                    binding.searchView.isIconified = true
                }
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                return true
            }
        })
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    @SuppressLint("MissingPermission")
    fun getCurrentLocationForWeatherData() {
        Log.e("TAG","In getCurrentLocationForWeatherData")
        fusedLocationClient?.lastLocation?.addOnSuccessListener {
            viewModel.getDataFromCoordinate(Location(it.latitude, it.longitude))
        }
    }
}