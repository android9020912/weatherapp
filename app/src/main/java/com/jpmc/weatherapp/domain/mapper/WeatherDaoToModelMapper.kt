package com.jpmc.weatherapp.domain.mapper

import com.jpmc.weatherapp.common.Mapper
import com.jpmc.weatherapp.data.model.WeatherInfoResponse
import com.jpmc.weatherapp.domain.model.Location
import com.jpmc.weatherapp.domain.model.WeatherInfoModel

class WeatherDaoToModelMapper : Mapper<WeatherInfoResponse,WeatherInfoModel> {
    override fun mapFrom(from: WeatherInfoResponse): WeatherInfoModel {
        return WeatherInfoModel(location = Location(from.coord.lat, from.coord.lon),
            cityName = from.name,
            weatherDescription = from.weather.first().description,
            weatherID = from.weather.first().id,
            weatherMain = from.weather.first().main,
            temp = from.main.temp,
            tempMin = from.main.temp_min,
            tempMax = from.main.temp_max,
            feelsLike = from.main.feels_like,
            iconID = from.weather.first().icon,
            humidity = from.main.humidity,
            windSpeed = from.wind.speed)
    }
}