package com.jpmc.weatherapp.domain.repository

import com.jpmc.weatherapp.data.model.GeoDirectResponse
import com.jpmc.weatherapp.data.model.WeatherInfoResponse
import com.jpmc.weatherapp.domain.model.Location

interface IWeatherRepository {
    suspend fun getWeatherDetails(location: Location): WeatherInfoResponse
    suspend fun getGeoCode(cityName:String): GeoDirectResponse
}