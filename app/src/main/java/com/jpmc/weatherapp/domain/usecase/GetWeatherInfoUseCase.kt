package com.jpmc.weatherapp.domain.usecase

import com.jpmc.weatherapp.domain.mapper.WeatherDaoToModelMapper
import com.jpmc.weatherapp.domain.repository.IWeatherRepository
import com.jpmc.weatherapp.domain.model.Location
import com.jpmc.weatherapp.domain.model.WeatherInfoModel
import javax.inject.Inject

class GetWeatherInfoUseCase @Inject constructor(private val weatherRepository: IWeatherRepository) {

    suspend operator fun invoke(location: Location): WeatherInfoModel {
        val weatherInfoResponse = weatherRepository.getWeatherDetails(location)
        return WeatherDaoToModelMapper().mapFrom(weatherInfoResponse)
    }
}