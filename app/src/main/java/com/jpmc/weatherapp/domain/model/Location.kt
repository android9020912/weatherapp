package com.jpmc.weatherapp.domain.model

data class Location(val latitude: Double, val longitude: Double)
