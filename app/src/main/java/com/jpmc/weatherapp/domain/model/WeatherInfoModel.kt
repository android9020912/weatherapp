package com.jpmc.weatherapp.domain.model

import com.jpmc.weatherapp.utils.Constants.ICON_URL

data class WeatherInfoModel(
    val location: Location,
    val weatherDescription: String,
    val cityName: String,
    val weatherID: Int,
    val weatherMain: String,
    val iconID: String,
    val temp: Double,
    val tempMin: Double,
    val tempMax: Double,
    val feelsLike: Double,
    val windSpeed: Double,
    val humidity: Int
) {
    val weatherIconURL: String
        get() = "$ICON_URL${iconID}@2x.png"
}

