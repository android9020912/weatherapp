package com.jpmc.weatherapp.domain.usecase

import com.jpmc.weatherapp.data.local.StateStoreManager
import com.jpmc.weatherapp.domain.repository.IWeatherRepository
import com.jpmc.weatherapp.domain.model.Location
import javax.inject.Inject

class GetGeoCodeUseCase @Inject constructor(
    private val weatherRepository: IWeatherRepository,
    private val stateStoreManager: StateStoreManager
) {
    suspend operator fun invoke(cityName: String): Location {
        val geoCodeResponse = weatherRepository.getGeoCode(cityName)
        //here i am picking first data from the response result
        //but given more time or based on UI requirement we can give option to user to select city based on the response and then display result

        val firstLat = geoCodeResponse.first().lat
        val firstLong = geoCodeResponse.first().lon
        stateStoreManager.saveLocation(firstLat.toFloat(), firstLong.toFloat())
        return Location(firstLat, firstLong)
    }
}