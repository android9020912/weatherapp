package com.jpmc.weatherapp.di

import android.content.Context
import android.content.SharedPreferences
import com.jpmc.weatherapp.data.IStateStore
import com.jpmc.weatherapp.data.WeatherAPI
import com.jpmc.weatherapp.data.local.StateStoreManager
import com.jpmc.weatherapp.data.repository.WeatherRepositoryImpl
import com.jpmc.weatherapp.domain.repository.IWeatherRepository
import com.jpmc.weatherapp.utils.Constants
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    @Singleton
    fun provideClient(): OkHttpClient = OkHttpClient.Builder()
        .connectTimeout(60 * 60, TimeUnit.SECONDS) // /60*30
        .readTimeout(60 * 60, TimeUnit.SECONDS)
        .writeTimeout(60 * 60, TimeUnit.SECONDS).build()

    @Provides
    @Singleton
    fun provideApi(client: OkHttpClient): WeatherAPI = Retrofit.Builder()
        .baseUrl(Constants.BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .client(client)
        .build().create(WeatherAPI::class.java)

    @Provides
    fun provideWeatherRepository(weatherApi: WeatherAPI): IWeatherRepository {
        return WeatherRepositoryImpl(weatherApi)
    }

    @Singleton
    @Provides
    fun provideSharedPreference(@ApplicationContext context: Context): SharedPreferences {
        return context.getSharedPreferences("Location", Context.MODE_PRIVATE)
    }

    @Singleton
    @Provides
    fun provideStateStoreManager(preferences: SharedPreferences): IStateStore {
        return StateStoreManager(preferences)
    }
}