package com.jpmc.weatherapp.common

interface Mapper<F,T> {
    fun mapFrom(from:F):T
}